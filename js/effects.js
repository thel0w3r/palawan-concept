$(document).ready(function () {
    //Menu Effect
    $('.menu a').each(function (index, element) {
        $(this).css({
            'top': '-200px'
        });
        $(this).animate({
            'top': '0'
        }, 2000 + (index * 500));
    });
    //----
    //Header Effect
    if($(window).width() > 800) {
        $('header .intro').css({
            opacity: 0,
            marginTop: 0
        });
        $('header .intro').animate({
            opacity: 1,
            marginTop: '-52px'
        }, 1500);
    }
    //----
    //Scroll Effect
    var about = $('#about').offset().top - 200,
        menu = $('#menutitle').offset().top,
        photos = $('#photos').offset().top,
        location = $('#location').offset().top;

    $('#btn-about').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: about
        }, 500);
    });
    $('#btn-menu').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate( {
            scrollTop: menu
        }, 500);
    });
    $('#btn-photos').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: photos
        }, 500);
    });
    $('#btn-location').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate( {
            scrollTop: location
        }, 500);
    });
    //----
});
